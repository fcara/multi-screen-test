using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    [SerializeField]
    private float speed;
    private float xMove;

    // Update is called once per frame
    void Update()
    {
        xMove = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate() {
        transform.position += Vector3.right * xMove * Time.fixedDeltaTime * speed;
    }
}
