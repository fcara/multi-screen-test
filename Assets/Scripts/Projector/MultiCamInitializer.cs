﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiCamInitializer : MonoBehaviour
{
    [SerializeField]
    private int projectionCams = 1;

    [SerializeField]
    private Vector2 gameSize = new Vector2(64,9);
    [SerializeField]
    private Vector2 center;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Camera refCamera;

    [SerializeField]
    private BlackBorderCanvas blackBorderRef;

    void Start()
    {

        CameraManager camManager = CameraManager.Instance;
        if (!camManager.Modified) {
            for (int i = 0; i < projectionCams; i++) {
                camManager.AddConfig(new CameraConfig(camManager.RefConfig.Width, camManager.RefConfig.Height));
                if(i%2==0)
                camManager.GetConfig(i).ReducedWidth /= 2;
            }
        }

        mainCamera.gameObject.SetActive(camManager.MainCameraOn);


        float cameraWidth = gameSize.x/camManager.Count;
        float startPosition = center.x-gameSize.x/2+cameraWidth/2;
        int displayCount = Display.displays.Length;
        int accumulatedWidth = 0;
        Camera currentCamera;
        for (int i = 0; i < camManager.Count; i++) {
            CameraConfig cameraConfig = camManager.GetConfig(i);
            currentCamera = Instantiate<Camera>(refCamera);
            currentCamera.transform.position = new Vector3(startPosition + accumulatedWidth + cameraWidth, currentCamera.transform.position.y, currentCamera.transform.position.z);
            currentCamera.targetDisplay = i+1;
            currentCamera.orthographicSize = cameraWidth / 2 / refCamera.aspect;
            if (i+1 < displayCount) {
                Display display = Display.displays[i + 1];
                display.SetRenderingResolution(cameraConfig.Width, (int)(cameraConfig.Width * gameSize.y / gameSize.x * camManager.Count));
                display.Activate();
            }
            if (cameraConfig.ReducedWidth < cameraConfig.Width) {
                SetBlackBorder(currentCamera, cameraConfig);
            }
            accumulatedWidth += Mathf.RoundToInt( cameraWidth * cameraConfig.ReducedWidth / cameraConfig.Width );
        }
    }

    private void SetBlackBorder(Camera currentCamera, CameraConfig cameraConfig) {
        BlackBorderCanvas blackBorderCanvas = Instantiate(blackBorderRef);
        blackBorderCanvas.name = $"{currentCamera.name} Black Border Canvas";
        blackBorderCanvas.Canvas.worldCamera = currentCamera;
        blackBorderCanvas.SetBlackBorderWidth(cameraConfig.ReducedWidth);
    }

    private void OnDrawGizmos() {
        Gizmos.color = new Color(0.2f,0.2f,1);
        Gizmos.DrawWireCube(center, gameSize);
    }
}
