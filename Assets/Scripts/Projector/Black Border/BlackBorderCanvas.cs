using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackBorderCanvas : MonoBehaviour {
    [SerializeField]
    private Canvas canvas;
    public Canvas Canvas => canvas;
    [SerializeField]
    private RectTransform blackBorder;

    public void SetBlackBorderWidth(int width) {
        blackBorder.sizeDelta = new Vector2(width, blackBorder.sizeDelta.y);
    }
}
