﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraGeneralConfigController : MonoBehaviour
{
    [SerializeField]
    private string gameScene;

    [SerializeField]
    private CameraConfigController mainCameraController;

    [SerializeField]
    private List<CameraConfigController> controllers;

    void Start() {
        CameraConfigController initialCam = controllers[0];
        controllers.ForEach(controller => {
            controller.CameraOn = false;
            controller.Width = 1920;
            controller.Height = 1080;
        });
        mainCameraController.CameraOn = true;
    }

    public void Confirm() {
        SetConfig();
        SceneManager.LoadScene(gameScene);
    }

    private void SetConfig() {
        CameraManager camManager = CameraManager.Instance;
        camManager.ClearPrevConfigs();
        controllers.ForEach(controller=> controller.AddConfig(camManager));
        camManager.MainCameraOn = mainCameraController.CameraOn;
    }
}
