﻿public class CameraConfig {

    public int Width { get; set; }
    public int Height { get; set; }

    public int ReducedWidth { get; set; }
    public int ReducedHeight { get; set; }

    public CameraConfig(int width, int height) {
        ReducedWidth = Width = width;
        ReducedHeight = Height = height;
    }

}