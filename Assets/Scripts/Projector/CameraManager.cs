﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager
{
    private static CameraManager instance;
    public static CameraManager Instance {
        get {
            if (instance == null)
                instance = new CameraManager();
            return instance;
        } 
    }

    private List<CameraConfig> configs = new List<CameraConfig>();

    public bool Modified { get; set; }

    public int Count => configs.Count;

    public bool MainCameraOn { get; set; } = true;
    public CameraConfig RefConfig { get; internal set; }

    private CameraManager() {
        RefConfig = new CameraConfig(1920,1080);
    }

    public CameraConfig GetConfig(int index) {
        return configs[index];
    }

    public void AddConfig(CameraConfig newConfig) {
        Modified = true;
        configs.Add(newConfig);
    }

    internal void ClearPrevConfigs() {
        configs.Clear();
        Modified = true;
    }
}
