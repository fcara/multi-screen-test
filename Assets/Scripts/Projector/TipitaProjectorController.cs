﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipitaProjectorController : MonoBehaviour {

    [SerializeField]
    public Animator animator;
    [SerializeField]
    public float runSpeed = 40f;
    [SerializeField]
    public float walkingDirectionX = 1;
    [SerializeField, Range(0, .3f)] 
    private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
    [SerializeField]
    private float movingLimits = 100;
    [SerializeField]
    private Transform startingPositionTransform;

    private float horizontalMove;
    private new Rigidbody2D rigidbody2D;
    private Vector3 m_Velocity = Vector3.zero;

    private void Awake() {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update() {
        horizontalMove = walkingDirectionX * runSpeed;
        animator.SetFloat("xSpeed", Mathf.Abs(horizontalMove));
        if (transform.position.x > movingLimits) {
            transform.position = startingPositionTransform.position;
        }
    }
    void FixedUpdate() {
        Move(horizontalMove * Time.fixedDeltaTime);
    }

    private void Move(float move) {
        // Move the character by finding the target velocity
        Vector3 targetVelocity = new Vector2(move * 10f, rigidbody2D.velocity.y);
        // And then smoothing it out and applying it to the character
        rigidbody2D.velocity = Vector3.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
    }

    public void PlayStepSfx() {
        /*if (stepEmitter != null) {
            stepEmitter.Play();
        }*/
    }
}
