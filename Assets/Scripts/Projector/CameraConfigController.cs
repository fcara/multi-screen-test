﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraConfigController : MonoBehaviour {
    [SerializeField]
    private Toggle cameraTgl;
    public bool CameraOn {
        get => cameraTgl.isOn;
        set => cameraTgl.isOn = value;
    }

    [SerializeField]
    private InputField widthInp;
    public int Width {
        get => int.Parse(widthInp.text);
        set => widthInp.text = value.ToString();
    }

    [SerializeField]
    private InputField heightInp; 
    public int Height {
        get => int.Parse(heightInp.text);
        set => heightInp.text = value.ToString();
    }

    public void AddConfig(CameraManager camManager) {
        if (!CameraOn)
            return;
        CameraConfig newConfig = new CameraConfig(Width,Height);
        camManager.AddConfig(newConfig);
    }
}
